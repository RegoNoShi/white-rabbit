
"""
Second solution for White Rabbit challenge - Anagrams generator
- Massimo Omodei
"""

import itertools
import os
from multiprocessing import Process


class Word:
    """ This class models a word as a characters occurrences dictionary """
    def __init__(self, w):
        self.word = w
        self.occ_dict = dict()
        for c in w:
            if c in self.occ_dict:
                self.occ_dict[c] += 1
            else:
                self.occ_dict[c] = 1

    def __cmp__(self, other):
        return 0 if len(self.word) == len(other.word) else len(self.word) - len(other.word)


class Choice:
    """ Model an anagram as a characters occurrences dictionary and a list of id of
        selected word in the wordlist 
    """
    wordlist = []

    def __init__(self, occ_dict, word_id_list):
        """ Id of next word to choose """
        self.occ_dict = occ_dict
        self.word_id_list = word_id_list

    def is_sub_word(self, word_id):
        """ Method to check if word is a subset """
        if len(Choice.wordlist[word_id].occ_dict) > len(self.occ_dict):
            return False
        for ch in Choice.wordlist[word_id].occ_dict.iterkeys():
            if ch not in self.occ_dict:
                return False
            if Choice.wordlist[word_id].occ_dict[ch] > self.occ_dict[ch]:
                return False
        return True

    def has_next_choice(self):
        """ Check if there in another word to choose """
        if self.word_id_list[-1] + 1 < len(Choice.wordlist) - 1:
            return True
        else:
            return False

    def get_next_choice(self):
        """ Get next choice """
        lst = self.word_id_list[:-1]
        lst.append(self.word_id_list[-1] + 1)
        return Choice(self.occ_dict, lst)

    def select_word(self, word_id):
        """ Add word_id to selected word """
        lst = self.word_id_list
        lst.append(word_id)
        res = Choice(dict(), lst)
        for ch in self.occ_dict.iterkeys():
            if ch not in Choice.wordlist[word_id].occ_dict:
                res.occ_dict[ch] = self.occ_dict[ch]
            elif self.occ_dict[ch] > Choice.wordlist[word_id].occ_dict[ch]:
                res.occ_dict[ch] = self.occ_dict[ch] - Choice.wordlist[word_id].occ_dict[ch]
        return res

    def get_last_word_id(self):
        """ Return last word in selected word list """
        return self.word_id_list[-1]


def get_occ_dict(w):
    """ Given a word calculate characters occurrences dictionary """
    d = dict()
    for ch in w:
        if ch in d:
            d[ch] += 1
        else:
            d[ch] = 1
    return d


def is_sub_word(od1, od2):
    """ Given two characters occurrences dictionaries check if the first is a subset of the second """
    for ch in od1.iterkeys():
        if ch not in od2:
            return False
        if od1[ch] > od2[ch]:
            return False
    return True


def generate_anagrams(thread_id, threads_n, original_od, wl, a_path, a_per_file):
    """ Function for multithread anagrams generation
        Every thread generate anagrams starting with a range of words
        Words range is determined based on thread_id and thread_num
        original_od is the characters occurrences dictionary of original phrase
        wl is the cleaned wordlist
        a_path is the anagrams directory
        a_per_file is the number of anagrams per file
    """
    # Used in file name
    step = 0
    # Start and end words id
    start = (len(wl) / threads_n) * thread_id
    end = ((len(wl) / threads_n) * (thread_id + 1)) - 1
    count = a_per_file
    Choice.wordlist = wl
    # Starting choice
    base_choice = Choice(original_od, [start])
    # Create dir if not exists
    if not os.path.exists(a_path):
        os.makedirs(a_path)
    # Temporary file name
    tmp_file = os.path.join(a_path, 'tmp-{}')
    # Final file name
    anagrams_file = os.path.join(a_path, 'anagrams-{}-{}')
    anagrams = open(tmp_file.format(thread_id), "w")
    # Choices list (LIFO)
    choices = [base_choice]
    while len(choices) > 0:
        # Remove last choice
        choice = choices.pop()
        # Check if end is reached
        if len(choice.word_id_list) == 1:
            if choice.get_last_word_id() > end:
                break
            else:
                print "Thread: {} - WordID: {}".format(thread_id, choice.get_last_word_id())
        # Check if there is next choice
        if choice.has_next_choice():
            # If yes add to the end of the list
            choices.append(choice.get_next_choice())
        # Check if last word is a subset of anagram
        if choice.is_sub_word(choice.get_last_word_id()):
            # If yes select word and calculate next choice
            new_choice = choice.select_word(choice.get_last_word_id())
            # If dictionary is empty the word list is an anagra
            if len(new_choice.occ_dict) == 0:
                # Calculate all permutations of words
                for p in itertools.permutations(new_choice.word_id_list):
                    # Write anagram to file
                    anagrams.write(" ".join(map(lambda x: Choice.wordlist[x].word, p)) + "\n")
                    # Update count and check if file must be closed
                    count -= 1
                    if count == 0:
                        anagrams.close()
                        # Rename file for md5_check.py
                        os.rename(tmp_file.format(thread_id), anagrams_file.format(step, thread_id))
                        print "Thread: {} - Closed file: {}".format(thread_id, step)
                        # Open new file and reset counter
                        step += 1
                        anagrams = open(tmp_file.format(thread_id), "w")
                        count = a_per_file
            # Anagram not, add new_choice to list
            else:
                choices.append(new_choice)
    # Close and rename last file
    anagrams.close()
    os.rename(tmp_file.format(thread_id), anagrams_file.format(step, thread_id))
    print "Thread: {} - Closed file: {}".format(thread_id, step)

# Main thread
if __name__ == '__main__':
    # Original phrase without spaces
    original = "poultryoutwitsants"
    # Characters occurrences dictionary of original phrase
    original_occ_dict = get_occ_dict(original)
    anagrams_path = 'anagrams'
    anagrams_per_file = 100000000
    # Read wordlist file in a set for duplicated words removal
    # Filter wordlist: words with letters not in base phrase (or with too many occurrences)
    with open("wordlist") as f:
        wordlist = sorted(filter(lambda w: is_sub_word(w.occ_dict, original_occ_dict),
                                 map(Word, set(f.read().splitlines()))), reverse=True)
    # Processes list and processes/threads number
    processes = []
    threads_num = 4
    # Start threads/processes
    for th_id in xrange(threads_num):
        processes.append(Process(target=generate_anagrams, args=(th_id, threads_num, original_occ_dict, wordlist,
                                                                 anagrams_path, anagrams_per_file)))
        processes[-1].start()

    for process in processes:
        process.join()
