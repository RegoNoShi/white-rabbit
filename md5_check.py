
"""
Second solution for White Rabbit challenge - Anagrams checker
- Massimo Omodei
"""

import subprocess
import os
import time

# Anagrams files directory
anagrams_path = 'anagrams'
# Hashcat binaries directory
hash_cat_path = 'hashcat-3.40'
# Pot file
pot_file = os.path.join(hash_cat_path, 'hashcat.potfile')
# Hashes to crack
hashes_file = 'hashes.in'
# Execute: hashcat64.exe -m 0 hashes_file anagrams_files_list
hash_cat_command = [os.path.join(hash_cat_path, 'hashcat64.exe'), "-m", "0", hashes_file]
# Redirect log to dev null
dev_null = open(os.devnull, 'w')
# Create pot file if not exists
with open(pot_file, 'a'):
    os.utime(pot_file, None)
# Count hashes to crack
with open(hashes_file) as ha:
    hashes_num = len(ha.read().splitlines())
# Until all hashes has not been cracked
finished = False
while not finished:
    # Search anagrams files in anagrams directory
    anagrams_files = filter(lambda f: f.startswith('anagrams'), os.listdir(anagrams_path))
    # Get full path for anagrams files
    anagrams_files = map(lambda f: os.path.join(anagrams_path, f), anagrams_files)
    # If there is at least 1 anagrams file
    if len(anagrams_files) > 0:
        # Make a copy of command list
        command = list(hash_cat_command)
        # Extend command with anagrams files list
        command.extend(anagrams_files)
        # Exec command and redirect output and errors to dev null
        print "Executing:\t\t{}".format(" ".join(command))
        subprocess.call(command, stdout=dev_null, stderr=subprocess.STDOUT)
        # Remove checked anagrams files
        for path in anagrams_files:
            os.remove(path)
        # Check if all hashes has been cracked
        with open(pot_file) as an:
            cracked = len(an.read().splitlines())
        finished = cracked >= hashes_num
        print "{}/{}".format(cracked, hashes_num)
    # Sleep for 5 seconds waiting for new anagrams files
    else:
        print "Sleeping for 5 seconds"
        time.sleep(5)
# When finished print pot file content
with open(pot_file) as an:
    for anagram in an.read().splitlines():
        print anagram
