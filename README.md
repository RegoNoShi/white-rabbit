# follow the white rabbit

## Solving the challenge from TrustPilot

## [Git Repository](https://bitbucket.org/RegoNoShi/white-rabbit)

### Core Ideas:
Remove from wordlist:  

- Words with letters not in base phrase (or with too many occurrences)  
- Words with single quotes  
- Duplicated words  

### First Solution (white_rabbit_1.py):
At first I hoped that the anagrams would be composed by only 3 words.  
So I made a simple Python script that after cleaning the wordlist calculate all 3 words combinations.   
For each combinations of 3 words I check if the concatenation is an anagram of base phrase by sorting all letters and comparing with the sorted base phrase (without spaces).   
If is an anagram I calculate all permutations of the 3 words, determine the md5 hash and compare it with the 3 given hashes.  
Unfortunately only the first two anagrams are composed by 3 words.   
  
- Easiest secret phrase: printout stout yawls  
- More difficult secret phrase: ty outlaws printouts  

### Second Solution (white_rabbit_2.py and md5_check.py):
In the second solution I've divided the anagrams generation part from the MD5 hash generation and verification part.  
I made a Python script that after cleaning the wordlist calcute all possible group of words that are anagrams using the words in wordlist ordered from longest to shortest.   
For the anagrams generation and check I've modeled words and anagrams as character occurrence dictionaries and checked inclusions and intersections.  
For each combinations of words that is an anagram of the base phrase I calculate and write to file all permutations.   
I've parallellized the anagrams generation and I close a file when a given number of anagrams have been written.  
With another Python script I invoke hashcat to verify all anagrams written to file using the raw power of my video card.  
After some hours of computation finally I've found the third anagram!  
  
- Hard secret phrase: wu lisp not statutory  

### Notes:
I've used hashcat-3.40  
Put hashcat binaries wherever you want and change hash_cat_path in md5_check.py  
