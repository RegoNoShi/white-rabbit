
"""
First solution for White Rabbit challenge
- Massimo Omodei
"""

import itertools
from hashlib import md5


def get_occ_dict(w):
    """ Given a word calculate characters occurrences dictionary """
    d = dict()
    for ch in w:
        if ch in d:
            d[ch] += 1
        else:
            d[ch] = 1
    return d


def is_sub_word(od1, od2):
    """ Given two characters occurrences dictionaries check if the first is a subset of the second """
    for ch in od1.iterkeys():
        if ch not in od2:
            return False
        if od1[ch] > od2[ch]:
            return False
    return True

# Original phrase without spaces
original = "poultryoutwitsants"
# Sorted original phrase
original_sorted = "".join(sorted(original))
# Characters occurrences dictionary of original phrase
original_od = get_occ_dict(original)
# Hashes given
easy = "e4820b45d2277f3844eac66c903e84be"
diff = "23170acc097c24edb98fc5488ab033fe"
hard = "665e5bcb0c20062fe8abaaf4628bb154"

# Read wordlist file in a set for duplicated words removal
with open("wordlist") as f:
    wordlist = set(f.read().splitlines())
# Filter wordlist: words with letters not in base phrase (or with too many occurrences)
wordlist = filter(lambda wo: is_sub_word(get_occ_dict(wo.strip()), original_od), wordlist)
# Numbers of words in anagrams
words_num = 3
# Calculate all combinations with replacament
for c in itertools.combinations_with_replacement(wordlist, words_num):
    # Concatenate words
    comb = "".join(c)
    # Check if len is equal to base phrase and if sorted tmp is equal to sorted original phrase
    # If all two checks return true the combination is a valid anagram
    if len(comb) == len(original_sorted) and "".join(sorted(comb)) == original_sorted:
        # Calculate all permutations of words
        for p in itertools.permutations(c):
            # Concatenate words separated by spaces
            perm = " ".join(p)
            # Calculate MD5 hash
            h = md5(perm).hexdigest()
            # Compare with given hashes
            if h == easy:
                print "EASY: \t\t{}".format(perm)
            if h == diff:
                print "DIFF: \t\t{}".format(perm)
            if h == hard:
                print "HARD: \t\t{}".format(perm)
